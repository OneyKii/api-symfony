<?php

namespace App\Controller;

use App\Entity\Scores;
use InvalidArgumentException;
use App\Repository\ScoresRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ScoresController extends AbstractController
{

        #[Route('/info', name: 'serializer', methods: ['GET'])]
        public function scores(ScoresRepository $scoresRepository): Response
        {
                return $this->json($scoresRepository->findAllOrderByScore(),200, [], ['groups' => 'scores:read']);
        }

        #[Route('/info/post', name: 'serializer_add', methods: ['POST'])]
        public function addScore(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, 
        ValidatorInterface $validator)
        {
       
            $jsonRecu = $request->getContent();

            $score = $serializer->deserialize($jsonRecu, Score::class, 'json');

            $score->setCreatedAt(new \DateTimeImmutable());

            /* $errors = $validator->validate($score); */

            /* if(count($errors) > 0){
                return $this->json($errors, 400);
            } */

            $em->persist($score);
            $em->flush();

            return $this->json($score, 201, ['groups' =>'score:read']);
        }


    /* #[Route('/info/{id}', name: 'serializer_id',  methods:'GET')] 
    public function getInformationById(ScoresRepository $scoresRepository, int $id) 
    {
        $score = $scoresRepository->find($id); 
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($score, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        $response = new Response($jsonContent);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    } */
    #[Route('/info/{id}', name: 'serializer_delete', methods: ['DELETE'])]
    public function deleteScoresbyId (EntityManagerInterface $em, ScoresController $scoresController, int $id){

        try {
            $scores = $scoresController->findOneBy(['id' => $id]);
            $em->remove($scores);
            $em->flush();
            return $this->json([
                'status' => 200,
                'message' => 'Score supprimÃ©'
            ]);
        } catch(NotFoundHttpException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ]);
        } catch(InvalidArgumentException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ]);
        }
   }
}
