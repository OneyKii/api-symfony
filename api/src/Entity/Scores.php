<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ScoresRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ScoresRepository::class)
 * @ApiResource(
 *          normalizationContext={"groups"={"scores:read"}},
 *          denormalizationContext={"groups"={"scores:write"}},
 * )
 */
class Scores
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("scores:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"scores:write", "scores:read"})
     * @Assert\NotBlank
     */
    private $pseudo;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"scores:write", "scores:read"})
     * @Assert\NotBlank
     */
    private $score;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Groups("scores:read")
     */
    private $CreatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->CreatedAt;
    }

    public function setCreatedAt(\DateTimeImmutable $CreatedAt): self
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }
}
